const express = require('express');
const bodyParser = require('body-parser');
const pino = require('express-pino-logger')();
const fs = require('fs');
const filter = require('lodash/filter');
const axios = require('axios');
const find = require('lodash/find');
const stringify = require('json-stringify-safe');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())
app.use(pino);

app.get('/api/mockproxy/campaigns', (req, res) => {
    const { page, offset } = req.query;

    //console.log(req);
    const fileName = 'campaigns';

    res.setHeader('Content-Type', 'application/json');

    const allCampaignsString = getJSON(fileName);

    if (page != undefined && offset != undefined) {
        const allCampaigns = JSON.parse(allCampaignsString);
        const offsetCampaigns = getOffset(allCampaigns, parseInt(page), parseInt(offset));
        const offsetCampaignsString = JSON.stringify(offsetCampaigns);

        res.send(offsetCampaignsString);
    }
    else {
        res.send(allCampaignsString);
    }
});

app.get('', (req, res) => {
    //const { fileName } = req.query;
    console.log(req);
    const fileName = 'campaigns';
    const fileName2 = 'campaigns_past';
    res.setHeader('Content-Type', 'application/json');
    console.log("fdfsd");

    const asd = getJSON(fileName);
    console.log(asd);
    res.send(getJSON(fileName));

    // if(req.query.status == "running"){
    //     res.send(getJSON(fileName));
    // }else if(req.query.status == "past"){
    //     res.send(getJSON(fileName2));
    // }
});

app.get('/api/mockproxy/profile', (req, res) => {
    //const { fileName } = req.query;
    const fileName = 'profile';
    res.setHeader('Content-Type', 'application/json');
    res.send(getJSON(fileName));
});

app.post('/api/mockproxy/campaigns', (req, res) => {
    const fileName = 'campaigns';
    logHeaderBody(req);
    let newCampaign = req.body;
    var dateTime = new Date();
    newCampaign.id = "" + dateTime.getTime();
    // console.log(req.body);
    let allCampaigns = JSON.parse(getJSON(fileName));
    allCampaigns.push(req.body.data);
    writeJSON(fileName, '', allCampaigns)
    res.setHeader('Content-Type', 'application/json');
    res.send(getJSON(fileName));
});

app.put('/api/mockproxy/campaigns', (req, res) => {
    const fileName = 'campaigns';
    logHeaderBody(req);
    const campaignToBeUpdated = req.body;
    const allCampaigns = JSON.parse(getJSON(fileName));
    let temp = deleteObject(allCampaigns, campaignToBeUpdated, 'id');
    temp.push(campaignToBeUpdated);
    writeJSON(fileName, '', temp)
    res.setHeader('Content-Type', 'application/json');
    //res.send(getJSON(fileName));
    res.statusCode = "200";
    res.send(genericSuccessResponse());
});

app.delete('/api/mockproxy/campaigns/:id', (req, res) => {
    const fileName = 'campaigns';
    logHeaderBody(req);

    const requestUrl = req.url.split('/');
    const id = requestUrl[requestUrl.length - 1];
    const allCampaigns = JSON.parse(getJSON(fileName));

    console.log('ID: ' + id);

    let temp = deleteObject(allCampaigns, { "id": id }, 'id');
    //let temp = deleteObjectById(allCampaigns, id);

    writeJSON(fileName, '', temp)
    res.setHeader('Content-Type', 'application/json');
    res.statusCode = "200";
    res.send(genericSuccessResponse());
    //res.send(getJSON(fileName));
});

app.listen(3001, () => console.log('Express server is running on localhost:3001'));

function genericSuccessResponse() {
    return {
        "status": "success",
        "code": 200
    }
}

function getJSON(fileName) {
    console.log(`folder path - ${__dirname}\\mock\\data\\${fileName}.json`);
    return fs.readFileSync(`${__dirname}\\mock\\data\\${fileName}.json`);
}

function writeJSON(fileName, key, value) {
    const payload = JSON.stringify(value);
    const filePath = `${__dirname}\\mock\\data\\${fileName}.json`;
    fs.writeFileSync(filePath, payload);
}

function logHeaderBody(req) {
    console.log("---REQUEST-HEADERS--------------------------------------------");
    console.log(req.headers);
    console.log("---REQUEST-BODY-----------------------------------------------");
    console.log(req.body);
    console.log("--------------------------------------------------------------");
}

function deleteObject(array, element, attribute) {
    let temp = array.filter(function (item) {
        return item[attribute] !== element[attribute];
    });
    return temp;
}

function getOffset(array, page, offset) {

    const startIndex = page * offset;
    const endIndex = startIndex + offset;

    console.log("startIndex " + startIndex);
    console.log("endIndex " + endIndex);


    return array.slice(startIndex, endIndex);

}

function deleteObjectById(array, id) {

    console.log(id)

    let idToBeDeleted = id;

    let temp = array.filter(function (item) {
        return item.id !== idToBeDeleted;
    });

    console.log(temp);

    return temp;
}